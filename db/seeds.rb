# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create(name:"copy",
                  email:"1Q@qq.com" ,
                  password:"linlin",
        password_confirmation:"linlin",
        admin:true
)
99.times do |n|
  name = Faker::Name.name
  email= "example-#{n+1}@qq.com"
  password="password"
  User.create!(name:name,
              email:email,
  password:password,
  password_confirmation:password
  )

  users = User.order(:created_at).take(6)
  50.times do
    content = Faker::Lorem.sentence(5)
    users.each{|user| user.microposts.create!(content: content)}
  end
end