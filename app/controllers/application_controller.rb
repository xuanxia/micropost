class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  def hello
    render text: "hello,world!"
  end
  module ApplicationHelper
    def full_title(page_title ='')
      base_title = "Ruby on Rails Sample App "
      if page_title.empty?
        base_title
      else
        "#{page_title}|#{base_title}"
      end
    end
  end

  private

  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Please log in"
      redirect_to login_url
    end
  end

end
