class StaticPagesController < ApplicationController
  def home
    if logged_in?
    @micropost = current_user.microposts.build
    @feed_items = current_user.feed.paginate(page: params[:page])
  end
end
  def help
  end
  def about

  end
  def connect
  end
  def news

  end
  def changetx

  end
end
