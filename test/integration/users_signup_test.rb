require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
 test "invalid signup information" do
   get sigup_path
   name ="Examplename"
   email="123asd@qq.com"
   password ="password"
   assert_difference 'User.count',1 do
     post user_path,user:{
                       name: name,
                       email:email,
                       password:             password,
                       password_confirmation:password
                   }
   end
   assert_template  'users/show'
 end

end
