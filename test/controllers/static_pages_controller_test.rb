require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "home| Ruby on Rails Sample App"
  end

  test "should get help" do
    get :help
    assert_response :success
    assert_select "title", "help| Ruby on Rails Sample App"
  end
 test "should get about "do
   get :about
   assert_response :success
   assert_select "title", "about| Ruby on Rails Sample App"
 end
  test "should get connect "do
    get :connect
    assert_response :success
    assert_select "title", "connect| Ruby on Rails Sample App"
  end
end
